import TestService from '../services/TestService.js';

const test = async (req, res) => {
    const result = await TestService.getOne();
    res.json(result);
};

export default {
    test
}