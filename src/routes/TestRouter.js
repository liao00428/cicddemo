import { Router } from "express"
import TestController from '../controllers/TestController.js';  

const router = Router();
router.route('/test')
    .get(TestController.test)

export default router;