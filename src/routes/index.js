import { Router } from "express"
import TestRouter from './TestRouter.js';

const router = Router();
router.use('/', TestRouter);

export default router;