import TestModel from '../models/TestModel.js';

const getOne = async () => {
    const result = await TestModel.getOne();
    return result;
};

export default {
    getOne
};